﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using TicTocGame.Properties;
using System.Windows.Forms;
using System.Media;

namespace TicTocGame
{
    class GridGraph
    {

        private static Graphics Graphics;
        public GridGraph(Graphics g) 
        {
           Graphics = g;
            drawGrid();
        }

       public static void drawGrid() 
        {

            Brush backGround = new SolidBrush(Color.GhostWhite);
            Pen Lines = new Pen(Color.Black,5);

            Graphics.FillRectangle(backGround, new Rectangle(0,0, 500,600));
         
            //Hor Lines
            Graphics.DrawLine(Lines, new Point(167, 0), new Point(167, 600));
            Graphics.DrawLine(Lines, new Point(334, 0), new Point(334, 600));

            //Ver Lines
            Graphics.DrawLine(Lines, new Point(0, 167), new Point(500, 167));
            Graphics.DrawLine(Lines, new Point(0,344), new Point(500,334));

            Graphics.DrawLine(Lines, new Point(0,500), new Point(500,500));
        }

        public static void DrawX(Point point) 
        {
            int xabs = point.X * 167;
            int yabs = point.Y * 167;
            Graphics.DrawImage(Image.FromFile("./Assets/BigBoss.png"), xabs + 10, yabs + 10, 147, 147);

        }
        public static void DrawO(Point point){
            int xabs = point.X * 167;
            int yabs = point.Y * 167;
            Graphics.DrawImage(Image.FromFile("./Assets/Demonsnake.png"), xabs + 10, yabs + 10, 147, 147);
          
        }
    }
}
