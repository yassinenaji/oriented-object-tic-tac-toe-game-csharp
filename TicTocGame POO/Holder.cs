﻿using System.Drawing;

namespace TicTocGame
{
      public  class Holder
    {


        private Point Location;
        private int value= Board.B;
        
        public Point GgetLocation() { return this.Location; }
        public int GgetValue() { return this.value; }
        public void SsetLocation(Point p) { this.Location = p; }
        public void SsetValue(int p) { this.value = p; }
    }
}
