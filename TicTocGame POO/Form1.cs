﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTocGame
{
    public partial class Form1 : Form
    {
        GridGraph Grid;
        Board board;
        public Form1()
        {
            InitializeComponent();
             board = new Board();
            this.BackgroundImage = Image.FromFile("./Assets/BackGround.jpg");


        }

  

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
           
            Graphics graphics = panel1.CreateGraphics();
            Grid = new GridGraph(graphics);
           
            board.InitBoard();
            refreshScore();
            
          
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            
            var curpos = Cursor.Position;
            curpos = panel1.PointToClient(curpos);
            
            board.DetectHit(curpos);
            refreshScore();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            board.Reset();
            GridGraph.drawGrid();
        }
        public void refreshScore() 
        {
          label1.Text=  board.RefreshScore();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            board.SaveMatch();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

          

        }

        private void button3_Click(object sender, EventArgs e)
        {
            board.Reset();
            GridGraph.drawGrid();

            board.LoadMatch();
            refreshScore();
        }
    }


}
