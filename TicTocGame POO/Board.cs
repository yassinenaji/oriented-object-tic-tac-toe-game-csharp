﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Media;
using System.Text.RegularExpressions;

namespace TicTocGame
{
    class Board
    {
        private Rectangle[,] rectangles = new Rectangle[3,3];
        private int     MovesMade;
        private Holder[,] holders = new Holder[3, 3];


        private int bigBossWins=0;

 

    

        private int demonSnakeWins=0;

   

        

        public const int X = 0;
        public const int O = 1;
        public const int B = 2;
        private int playerTurn = X;

  


  
       public Board() 
        {
            this.rectangles = new Rectangle[3, 3];
        }

        public void InitBoard() 
        {
            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                   
                    holders[x, y] = new Holder();
                    holders[x, y].SsetValue(B);
                    holders[x, y].SsetLocation(new Point(x, y));
                }

            }
        }

        //rest the board by clearing fields
        public void Reset()
        {
            holders = new Holder[3,3];
            InitBoard();
        }

        //detect if any hits is out of the board meanwhile counting moves and incrementing wins foreach player  
       public void DetectHit(Point point) 
        {

            int x = 0;
            int y = 0;
            if (point.Y <= 500 && point.X <= 500)
            {

                //x
                if (point.X < 167) { x = 0; }
                else if (point.X > 167 && point.X < 334)
                {
                    x = 1;
                }
                else if (point.X > 334)
                {
                    x = 2;
                }

                //y
                if (point.Y < 167)
                { y = 0; }
                else if (point.Y > 167 && point.Y < 334)
                {
                    y = 1;
                }
                else if (point.Y > 334 && point.Y < 500)
                {
                    y = 2;
                }
                SoundPlayer soundPlayer = new SoundPlayer();
                if (holders[x, y].GgetValue() == Board.B)
                {
                    if (MovesMade % 2 == 0)
                    {

                        GridGraph.DrawX(new Point(x, y));
                        holders[x, y].SsetValue(X);
                        if (DetectRow())
                        {


                            soundPlayer.Stream = Properties.Resources.snakeGameOver;

                            soundPlayer.Play();




                            var m = MessageBox.Show("Big Boss wins");
                            if (m == DialogResult.OK)
                            {
                                soundPlayer.Stop();
                            }
                            bigBossWins++;
                            Reset();
                            GridGraph.drawGrid();





                        }
                        playerTurn = O;

                    }
                    else
                    {


                        GridGraph.DrawO(new Point(x, y));
                        holders[x, y].SsetValue(O);
                        if (DetectRow())
                        {
                            soundPlayer.Stream = Properties.Resources.BigBossGameOver;

                            soundPlayer.Play();

                            var m = MessageBox.Show("Demon Snake wins");
                            if (m == DialogResult.OK)
                            {
                                soundPlayer.Stop();
                            }
                            demonSnakeWins++;
                            Reset();
                            GridGraph.drawGrid();



                        }

                        playerTurn = X;


                    }
                    if (!BoardIsFull())
                    {
                        MovesMade++;
                    }
                    else
                    {
                        soundPlayer.Stream = Properties.Resources.METAL;

                        soundPlayer.Play();
                        var m = MessageBox.Show("No one Wins");
                        if (m == DialogResult.OK)
                        {
                            soundPlayer.Stop();
                        }
                        Reset();
                        GridGraph.drawGrid();


                    }

                    //MessageBox.Show(x + " , " + y + "\n\n" + point.ToString());

                }
              
            }
            else { MessageBox.Show("you click out side of the box"); }

        
        }

        //determine if any of the victory conditions is comfirmed
        private bool DetectRow() 
        {
            bool isWon=false;
            for (int x = 0; x < 3; x++)
            {
                //detect Horizontal lignes For X and O
                if (holders[x, 0].GgetValue() == X && holders[x, 1].GgetValue() == X && holders[x, 2].GgetValue() == X)
                {
                    return true;
                }
                if (holders[x, 0].GgetValue() == O && holders[x, 1].GgetValue() ==O && holders[x, 2].GgetValue() == O)
                {
                    return true;
                }
                switch (x) {
                    case 0:
                        ////detect Diagonal lignes For X and O
                        if (holders[x,0].GgetValue()==X && holders[x+1, 1].GgetValue() == X && holders[x+2, 2].GgetValue() == X)
                        {
                            return true;
                        }
                        if (holders[x, 0].GgetValue() == O && holders[x + 1, 1].GgetValue() == O && holders[x + 2, 2].GgetValue() == O)
                        {
                            return true;
                        }

                        break;

                    case 2:
                        if (holders[x, 0].GgetValue() == X && holders[x - 1, 1].GgetValue() == X && holders[x - 2, 2].GgetValue() == X)
                        {
                            return true;
                        }
                        if (holders[x, 0].GgetValue() == O && holders[x - 1, 1].GgetValue() == O && holders[x - 2, 2].GgetValue() == O)
                        {
                            return true;
                        }

                        break;
                }
            }
            for (int y = 0; y < 3; y++)
            {
                //detect Vertical lignes For X and O
                if (holders[0, y].GgetValue() == X && holders[1, y].GgetValue() == X && holders[2, y].GgetValue() == X)
                {
                    return true;
                }
                if (holders[0, y].GgetValue() == O && holders[1, y].GgetValue() == O && holders[2, y].GgetValue() == O)
                {
                    return true;
                }
            }

            return isWon;
        
        }

        //verify if the board is full
        public bool BoardIsFull()
        {
            bool isfull = true;
            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    if (holders[x, y].GgetValue() == Board.B) 
                    {
                        return false;
                    }
                }

            }

            return isfull;
        }

        //saving match
        public void SaveMatch() 
        {
            
            string BigBossMoves = "Big Boss Mouvements: ";
            string DemonSnakeMoves = "Demon Snake Mouvements: ";

            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    if (holders[x, y].GgetValue() == Board.X)
                    {  
                        BigBossMoves += "(" + x + "," + y + ") ";

                    }
                    if (holders[x, y].GgetValue() == Board.O)
                    {
                         DemonSnakeMoves += "(" + x + "," + y + ") ";
                    }

                }
              




            }
            string RoundInfo =
                  DemonSnakeMoves +
                  " Rounds Wins:" + demonSnakeWins + "\n" +
                  BigBossMoves
                  + " Rounds Wins:" + bigBossWins;
            using (SaveFileDialog save=new SaveFileDialog())
            {
                
               // save.FileName = "Tic Tac Toc " + DateTime.Now.ToString();
               
                save.Filter = "Text files (*.txt)|*.txt";
                if (save.ShowDialog() == DialogResult.OK) { 
                File.WriteAllText(Path.GetFullPath(save.FileName),RoundInfo);
                    MessageBox.Show("Match saved successfully");
                }
            }
            
            


        }

        public string RefreshScore()
        {
            string turn;
            if (playerTurn == Board.X)
            {
                turn = "Big Boss's Turn";
            }
            else turn = "Demon Snake's Turn";
           return turn + "\n Score : Big Boss : " + demonSnakeWins.ToString() + " Vs " + "Demon Snake : " + bigBossWins.ToString();
        }

        //Loading a match
        public void LoadMatch() 
        {
            using (OpenFileDialog openFile = new OpenFileDialog())
            {

                openFile.Filter = "TXT files|*.txt";
                if (openFile.ShowDialog() == DialogResult.OK)
                {

                    try
                    {

                        Regex RegularExpCor = new Regex(@"\b(.,.)\b", RegexOptions.Multiline);
                        Regex RegularExpScore = new Regex(@"(Wins:.*)", RegexOptions.Multiline);


                        var file = openFile.FileName;
                        var result = File.ReadAllLines(file);
                        List<Point> BigBosscord = new List<Point>();
                        List<Point> DemonSnakeCor = new List<Point>();
                        int demonSnakeRw = 0;
                        int BigbossRw = 0;

                        foreach (var line in result)
                        {
                            Match Score = RegularExpScore.Match(line);
                            MatchCollection match = RegularExpCor.Matches(line);
                            string[] res;

                            List<Point> cor = new List<Point>();


                            int i = 0;
                            res = new string[match.Count];
                            foreach (Match item in match)
                            {
                                if (item.Success)
                                {
                                    res[i] = item.Value;
                                    i++;
                                }
                            }


                            foreach (var it in res)
                            {
                                var cr = it.Split(',');
                                cor.Add(new Point(int.Parse(cr[0].ToString()), int.Parse(cr[1].ToString())));

                            }
                            if (line.Contains("Demon"))
                            {
                                DemonSnakeCor.AddRange(cor);
                                var n = Score.Value.Split(':');
                                demonSnakeRw = int.Parse(n[1].ToString());
                            }
                            else
                            {

                                BigBosscord.AddRange(cor);
                                var n = Score.Value.Split(':');
                                BigbossRw = int.Parse(n[1].ToString());

                            }
                        }

                       
                    this.bigBossWins = BigbossRw;
                    for (int x = 0; x < BigBosscord.Count; x++)
                    {
                        GridGraph.DrawX(new Point(BigBosscord[x].X, BigBosscord[x].Y));
                        holders[BigBosscord[x].X, BigBosscord[x].Y].SsetValue(X);

                    }

                    this.demonSnakeWins = demonSnakeRw;
                        for (int x = 0; x < DemonSnakeCor.Count; x++)
                        {
                            GridGraph.DrawO(new Point(DemonSnakeCor[x].X, DemonSnakeCor[x].Y));
                            holders[DemonSnakeCor[x].X, DemonSnakeCor[x].Y].SsetValue(O);

                        }
                      
                        this.RefreshScore();



                }
                    catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }


            }
        }

    }


}
